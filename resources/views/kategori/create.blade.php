@extends('layout.master')

@section('judul')
Kategori Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Kategori Buku</a></li>
    <li class="breadcrumb-item"><a href="/kategori/show">Detail Buku</a></li>
    <li class="breadcrumb-item"><a href="/kategori/edit">Edit Buku</a></li>
  </ol>
@endsection

@section('content')

<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama</label>
      <input type="text" name="nama" class="form-control" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>




@endsection