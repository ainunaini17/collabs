@extends('layout.master')

@section('judul')
Detail Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/kategori">Kategori Buku</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail Buku</li>
    <li class="breadcrumb-item"><a href="/kategori/edit">Edit Buku</a></li>
  </ol>
@endsection

@section('content')

<h2>Show Genre {{$kategori->id}}</h2>
<h4>{{$kategori->nama}}</h4>
<div class="row">
  <div class="col-4"> 
    @foreach ($kategori->buku as $item)
    <div class="card" style="width: 18rem;">
      <img class="card-img-top" src="{{ asset('gambar/'. $item->sampul) }}" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">{{ $item->judul }}</h5>
        <p class="card-text">{{ $item->sinopsis }} </p> 
      </div>
</div>
    @endforeach
   
  </div>
</div>
@endsection