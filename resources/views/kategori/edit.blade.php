@extends('layout.master')

@section('judul')
Edit Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/kategori">Kategori Buku</a></li>
    <li class="breadcrumb-item"><a href="/kategori/show">Detail Buku</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Buku</li>
  </ol>
@endsection

@section('content')
{{-- <form action="/kategori/{{ $kategori->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-body">
        <div class="row">
            <div class="col-12">
                <div class="form-group has-icon-left">
                    <i class="fas fa-book-open"></i>
                    <label for="judul">Nama Kategori</label>
                    <div class="position-relative">
                        <input type="text" value="{{ $kategori->nama }}" name="nama" id="nama" class="form-control" placeholder="Masukan Nama Kategori" autofocus>
                    </div>
                    @error('nama')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>           
        </div>
    </div>
    <div class="modal-footer">
        <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button>
        <button type="submit" name="save" class="btn btn-primary">Save</button>
    </div>
</form> --}}
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
      <label >Nama</label>
      <input type="text" name="nama" value ="{{$kategori->nama}}" class="form-control" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

   

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection