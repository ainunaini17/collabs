@extends('layout.master')

@section('judul')
Detail Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/kategori">Kategori Buku</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail Buku</li>
    <li class="breadcrumb-item"><a href="/kategori/edit">Edit Buku</a></li>
  </ol>
@endsection

@section('content')


<h4>Kategori : {{$kategori->nama}}</h4>
{{-- <div class="row">
  <div class="col-4"> 
    @foreach ($kategori->buku as $item)
    <div class="card" style="width: 18rem;">
      <img class="card-img-top" src="{{asset('gambar/'. $item->sampul) }}" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">{{ $item->judul }}</h5>
        <p class="card-text">{{ $item->sinopsis }} </p> 
      </div>
    </div>
    @endforeach
   
  </div>
</div> --}}

<div class="row">
  @foreach ($kategori->buku as $item)
      
  
  <div class="col-2">
      <div class="card">
          <img src="{{asset('gambar/'.$item->sampul)}}" class="card-img-top" alt="...">
          <div class="card-body">
            <h3>{{$item->judul}}</h3>
            <p class="card-text">{{($item->sinopsis)}}</p> 
          </div>
      </div>
  </div>
  @endforeach
</div>     
@endsection