@extends('layout.master')

@section('judul')
Kategori Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Kategori Buku</a></li>
    <li class="breadcrumb-item"><a href="/kategori/show">Detail Buku</a></li>
    <li class="breadcrumb-item"><a href="/kategori/edit">Edit Buku</a></li>
  </ol>
@endsection

@section('content')

<button type="button" name="save" class="btn btn-primary mb-3" data-toggle="modal" data-target="#modal-item">Tambah Data</button>
{{-- <a href="/kategori/create" class="btn btn-primary my-2">Tambah</a> --}}
<div class="table-responsive p-3">
    <table class="table align-items-center table-flush" id="dataTable">
      <thead class="thead-light">
        <tr>
          <th>#</th>
          <th>Nama</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($kategori as $key=>$item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>
                <form action="/kategori/{{ $item->id }}" method="POST">
                    <a href="/kategori/{{ $item->id }}" class="btn btn-info btn-sm" role="button">Detail</a>
                    <a href="/kategori/{{ $item->id }}/edit" class="btn btn-warning btn-sm" role="button" >Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
            @empty
            <h1>Data Tidak Ada</h1>
        </tr>
        @endforelse
      </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="modal-item">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-item">Tambah Data Kategori</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/kategori" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-book-open"></i>
                                        <label for="nama">Nama Kategori</label>
                                        <div class="position-relative">
                                            <input type="text" name="nama" id="nama" class="form-control" placeholder="Isi Nama Kategori Buku" autofocus>
                                        </div>
                                        @error('nama')
                                            <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" name="save" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <a href="/kategori/create" class="btn btn-primary mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">List Buku</th>
        <th scope="col">Action</th>
        
      </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key => $item)

            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <ul>
                        @foreach ($item->buku as $value)
                        <li>{{$value->judul}}</li>
                        
                        @endforeach
                    </ul>

                </td>
              
                <td>
                    
                    <form action="/kategori/{{$item->id}}" method="POST">
                        <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm" >Detail</a>
                        <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm" >Edit</a>
                        @method('delete')
                        @csrf
                        
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data Masih Kosong</td>
            </tr>
        @endforelse
    </tbody>
  </table> --}}

@endsection