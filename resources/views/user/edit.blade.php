@extends('layout.master')

@section('judul')
Daftar User
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/user">Daftar User</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit User</li>
  </ol>
@endsection

@section('content')
    <form action="" method="POST">
        @csrf
        <div class="form-body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group has-icon-left">
                        <i class="fas fa-user"></i>
                        <label for="username">Username</label>
                        <div class="position-relative">
                            <input type="text" name="username" id="username" value="" class="form-control" placeholder="Isi Username User" autofocus>
                        </div>
                        @error('username')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group has-icon-left">
                        <i class="fas fa-envelope-open-text"></i>
                        <label for="email">Email</label>
                        <div class="position-relative">
                            <input type="text" name="email" id="email" value="" class="form-control" placeholder="Isi Email User" autofocus>
                        </div>
                        @error('email')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group has-icon-left">
                        <i class="fas fa-lock"></i>
                        <label for="password">password</label>
                        <div class="position-relative">
                            <input type="password" name="password" id="password" value="" class="form-control" placeholder="Isi Password User" autofocus>
                        </div>
                        @error('password')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group has-icon-left">
                        <i class="fas fa-clipboard-list"></i>
                        <label for="role">Role</label>
                        <div class="position-relative">
                            <select class="form-select form-control" name="role" id="role" value="">
                                <option selected disabled>Pilih Role</option>
                                <option value="admin">Admin</option>
                                <option value="guest">Guest</option>
                            </select>
                        </div>
                        @error('role')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button>
            <button type="submit" name="save" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection