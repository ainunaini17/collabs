@extends('layout.master')

@section('judul')
Daftar User
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Daftar User</a></li>
    <li class="breadcrumb-item"><a href="/user/edit">Edit User</a></li>
  </ol>
@endsection

@section('content')

<button type="button" name="save" class="btn btn-primary mb-3" data-toggle="modal" data-target="#modal-item">Tambah Data</button>
<div class="table-responsive p-3">
    <table class="table align-items-center table-flush" id="dataTable">
      <thead class="thead-light">
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Email</th>
          <th>Password</th>
          <th>Role</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>System Architect</td>
          <td>Edinburgh</td>
          <td>******</td>
          <td>Guest</td>
          <td>
            <form action="" method="POST">
                <a href="/user/edit" class="btn btn-warning btn-sm" role="button">Edit</a>
                @method('delete')
                @csrf
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
            </form>
        </td>
        </tr>
      </tbody>
    </table>
    <!-- Modal -->
    <div class="modal fade" id="modal-item">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-item">Tambah Data User</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-user"></i>
                                        <label for="username">Username</label>
                                        <div class="position-relative">
                                            <input type="text" name="username" id="username" class="form-control" placeholder="Isi Username User" autofocus>
                                        </div>
                                        @error('username')
                                            <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-envelope-open-text"></i>
                                        <label for="email">Email</label>
                                        <div class="position-relative">
                                            <input type="text" name="email" id="email" class="form-control" placeholder="Isi Email User" autofocus>
                                        </div>
                                        @error('email')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-lock"></i>
                                        <label for="password">password</label>
                                        <div class="position-relative">
                                            <input type="password" name="password" id="password" class="form-control" placeholder="Isi Password User" autofocus>
                                        </div>
                                        @error('password')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-clipboard-list"></i>
                                        <label for="role">Role</label>
                                        <div class="position-relative">
                                            <select class="form-select form-control" name="role" id="role">
                                                <option selected disabled>Pilih Role</option>
                                                <option value="admin">Admin</option>
                                                <option value="guest">Guest</option>
                                            </select>
                                        </div>
                                        @error('role')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" name="save" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection