@extends('layout.master')

@section('judul')
Profile
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/avawx7j9uutbapy7bhuajpy9ji0mmxfieoty1v8ur9mkpb3s/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: '#mytextarea'
    });
  </script>
  @endpush
@section('content')
{{-- <div> --}}
    <h2>Edit Profile {{$profile->id}}</h2>
    <form action="/profile/{{$profile->id}}" method="POST">
        @csrf
        @method('PUT')
        {{-- <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" class="form-control" name="name" value="{{$profile->user->name}}" disabled>
        </div>
        <div class="form-group">
            <label for="email">email</label>
            <input type="text" class="form-control" name="email" value="{{$profile->user->email}}" id="email"  disabled>
        </div> --}}
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" name="umur" value="{{$profile->umur}}" id="umur" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="nohp">No Telepon</label>
            <textarea name="nohp" class="form-control" id="mytextarea" >{{$profile->nohp}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" class="form-control" id="" >{{$profile->alamat}}</textarea>
            @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
{{-- </div> --}}
@endsection