@extends('layout.master')

@section('judul')
Detail Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/buku">Daftar Buku</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail Buku</li>
    <li class="breadcrumb-item"><a href="/buku/edit">Edit Buku</a></li>
  </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-2">
        <div class="card">
            <img src="{{asset('gambar/'.$buku->sampul)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <span class="badge badge-pill badge-info">{{$buku->kategori->nama}}</span>
              <h3>{{$buku->judul}}</h3>
              <p class="card-text">{{Str::limit($buku->sinopsis, 30)}}</p> 
             
            </div>
          </div>
    </div>
</div>

@endsection