@extends('layout.master')

@section('judul')
Detail Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/buku">Daftar Buku</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail Buku</li>
    <li class="breadcrumb-item"><a href="/buku/edit">Edit Buku</a></li>
  </ol>
@endsection

@section('content')

    <img src="{{asset('gambar/'. $buku->sampul)}}" class="img-fluid rounded-start" style="width:200px;height:200px;" alt="">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <b>Judul :</b>
            <p>{{ $buku->judul }}</p>

            <b>Sinopsis :</b>
            <p>{{ $buku->sinopsis }}</p>

            <b>Penulis :</b>
            <p>{{ $buku->penulis }}</p>
        </li>
    </ul>
@endsection