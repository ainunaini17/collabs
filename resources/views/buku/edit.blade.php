@extends('layout.master')

@section('judul')
Edit Buku
@endsection

{{-- @section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Daftar Buku</a></li>
    <li class="breadcrumb-item"><a href="#">Detail Buku</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Buku</li>
  </ol>
@endsection --}}

@section('content')
{{-- <form action="/buku/{{ $buku->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-body">
        <div class="row">
            <div class="col-12">
                <div class="form-group has-icon-left">
                    <i class="fas fa-book-open"></i>
                    <label for="judul">Judul Buku</label>
                    <div class="position-relative">
                        <input type="text" value="{{ $buku->judul }}" name="judul" id="judul" class="form-control" placeholder="Isi Judul Buku" autofocus>
                    </div>
                    @error('judul')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>
            <div class="col-12">
                <div class="form-group has-icon-left">
                    <i class="fas fa-user"></i>
                    <label for="penulis">Penulis</label>
                    <div class="position-relative">
                        <input type="text" value="{{ $buku->penulis }}" name="penulis" id="penulis" class="form-control" placeholder="Isi Penulis Buku" autofocus>
                    </div>
                    @error('penulis')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>
            <div class="col-12">
                <div class="form-group has-icon-left">
                    <i class="fas fa-align-left"></i>
                    <label for="sinopsis">Sinopsis</label>
                    <div class="position-relative">
                        <textarea name="sinopsis"  id="sinopsis" class="form-control">{{ $buku->sinopsis }}</textarea>
                    </div>
                    @error('sinopsis')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>
            <div class="col-12">
                <div class="form-group has-icon-left">
                    <i class="fas fa-image"></i>
                    <label for="sampul">Sampul</label>
                    <div class="position-relative">
                        <input type="file" name="sampul" id="sampul" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group has-icon-left">
                    <i class="fas fa-clipboard-list"></i>
                    <label for="kategori_id">Kategori</label>
                    <div class="position-relative">
                <select name="kategori_id" class="form-control form-select" id="kategori_id">
                    <option value="">---Pilih Kategori---</option>
                    @foreach ($kategori as $item)
                    @if ($item->id === $buku->kategori_id)
                    <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                        
                   @else
                    
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                    @endif   
                    @endforeach
                </select>
                    </div>
                    @error('kategori')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button>
        <button type="submit" name="save" class="btn btn-primary">Save</button>
    </div>
</form> --}}

    <form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
        @csrf

        @method('PUT')
        <div class="form-group">
            <label >Judul buku</label>
            <input type="text" name="judul" class="form-control" >
          </div>
          @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror             
      
          <div class="form-group">
            <label >Penulis</label>
            <input type="text" name="penulis" class="form-control" >
          </div>
          @error('penulis')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror 
      
          <div class="form-group">
              <label >Sinopsis</label>
              <textarea name="sinopsis"  class="form-control" cols="30" rows="10"></textarea>
          </div>
          @error('sinopsis')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
      
          <div class="form-group">
              <label >Sampul</label>
              <input type="file" name="sampul" class="form-control">
          </div>
          @error('sampul')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
  
      <div class="form-group">
          <label >Kategori</label>
          <select name="kategori_id" class="form-control" id="">
              <option value="">---Pilih Kategori---</option>
              @foreach ($kategori as $item)
                  @if ($item->id === $buku->kategori_id)
                  <option value="{{$item->id}}" selected>{{$item->nama}}</option>  
                  @else
                  <option value="{{$item->id}}" >{{$item->nama}}</option>  
                  @endif
              @endforeach
          </select>
        </div>
        @error('kategori_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
  
  
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  

@endsection