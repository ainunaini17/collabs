@extends('layout.master')

@section('judul')
Daftar Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Daftar Buku</a></li>
    <li class="breadcrumb-item"><a href="/buku/detail">Detail Buku</a></li>
    <li class="breadcrumb-item"><a href="/buku/edit">Edit Buku</a></li>
  </ol>
@endsection

@section('content')

{{-- <button type="button" name="save" class="btn btn-primary mb-3" data-toggle="modal" data-target="#modal-item">Tambah Data</button> --}}
@auth
<a href="/buku/create" class="btn btn-primary my-2">Tambah</a>   
@endauth

<div class="table-responsive p-3">
    <table class="table align-items-center table-flush" id="dataTable">
      <thead class="thead-light">
        <tr>
          <th>#</th>
          <th>Judul</th>
          <th>Penulis</th>
          <th>Kategori</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($buku as $key=>$item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->judul }}</td>
            <td>{{ $item->penulis }}</td>
            <td>{{ $item->kategori['nama'] }}</td>
            <td>
                <form action="/buku/{{ $item->id }}" method="POST">
                    <a href="/buku/{{ $item->id }}" class="btn btn-info btn-sm" role="button">Detail</a>
                    <a href="/buku/{{ $item->id }}/edit" class="btn btn-warning btn-sm" role="button" >Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
                </td>
            @empty
        </tr>
        @endforelse
      </tbody>
    </table>
    
    <!-- Modal -->
    {{-- <div class="modal fade" id="modal-item">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-item">Tambah Data Buku</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/buku" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-book-open"></i>
                                        <label for="judul">Judul Buku</label>
                                        <div class="position-relative">
                                            <input type="text" name="judul" id="judul" class="form-control" placeholder="Isi Judul Buku" autofocus>
                                        </div>
                                        @error('judul')
                                            <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-user"></i>
                                        <label for="penulis">Penulis</label>
                                        <div class="position-relative">
                                            <input type="text" name="penulis" id="penulis" class="form-control" placeholder="Isi Penulis Buku" autofocus>
                                        </div>
                                        @error('penulis')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-align-left"></i>
                                        <label for="sinopsis">Sinopsis</label>
                                        <div class="position-relative">
                                            <textarea name="sinopsis" id="sinopsis" class="form-control"></textarea>
                                        </div>
                                        @error('sinopsis')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-image"></i>
                                        <label for="sampul">Sampul</label>
                                        <div class="position-relative">
                                            <input type="file" name="sampul" id="sampul" class="form-control" >
                                        </div>
                                        @error('sampul')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-clipboard-list"></i>
                                        <label for="kategori">Kategori</label>
                                        <div class="position-relative">
                                        <select name="kategori_id" class="form-control form-select" id="">
                                            <option value="">---Pilih Kategori---</option>
                                            @foreach ($buku as $item)
                                            <option value="{{ $item->kategori_id }}">{{ $item->kategori['nama'] }}</option>
                                                
                                            @endforeach
                                        </select>
                                        
                                            
                                        </div>
                                        @error('kategori')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" name="save" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
</div>

{{-- <a href="/buku/create" class="btn btn-primary my-2">Tambah</a>

<div class="row">
    @forelse ($buku as $item)

    <div class="col-2">
        <div class="card">
            <img src="{{asset('gambar/'.$item->sampul)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <span class="badge badge-pill badge-info">{{$item->kategori->nama}}</span>
              <h3>{{$item->judul}}</h3>
              <p class="card-text">{{Str::limit($item->sinopsis, 30)}}</p> 
              @auth --}}
                  {{-- <form action="buku/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/buku/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  </form>
              @endauth --}}
               {{-- @guest
               <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
               @endguest --}}
             
            {{-- </div>
          </div>
    </div>
        
    @empty
        <h6>Data buku Belum Ada</h6>
    @endforelse

</div> --}}

@endsection