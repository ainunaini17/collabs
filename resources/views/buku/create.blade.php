@extends('layout.master')

@section('judul')
Halaman Tambah Buku
@endsection



@section('content')
<form action="/buku" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label >Judul buku</label>
      <input type="text" name="judul" class="form-control" >
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror   
    

    <div class="form-group">
      <label >Penulis</label>
      <input type="text" name="penulis" class="form-control" >
    </div>
    @error('penulis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 

    <div class="form-group">
        <label >Sinopsis</label>
        <textarea name="sinopsis"  id="mytextarea" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('sinopsis')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >Sampul</label>
        <input type="file" name="sampul" class="form-control">
    </div>
    @error('sampul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >Kategori</label>
        <select name="kategori_id" class="form-control" id="">
            <option value="">---Pilih Kategori---</option>
            @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>                
            @endforeach
        </select>
      </div>
      @error('kategori_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror 


    <button type="submit" class="btn btn-primary">Submit</button>
  </form>





@endsection