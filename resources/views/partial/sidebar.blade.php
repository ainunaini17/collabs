<!-- Sidebar -->
<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/dashboard">
      <div class="sidebar-brand-icon">
        <img src="{{asset('admin/img/logo/logo3.png')}}">
      </div>
      <div class="sidebar-brand-text mx-3">RuangBuku</div>
    </a>
    <hr class="sidebar-divider my-0">
    @auth
    <li class="nav-item">
      <a class="nav-link" href="/profile">
        <i class="fas fa-address-card"></i>
        <span>Profile</span></a>
    </li> 
    @endauth
    
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
      Dashboard
    </div>
    <li class="nav-item">
      <a class="nav-link" href="/dashboard">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
      Features
    </div>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseForm" aria-expanded="true"
        aria-controls="collapseForm">
        <i class="fas fa-book"></i>
        <span>Buku</span>
      </a>
      <div id="collapseForm" class="collapse" aria-labelledby="headingForm" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Buku</h6>
          <a class="collapse-item" href="/buku">Daftar Buku</a>
          @auth
          <a class="collapse-item" href="/kategori">Kategori</a>  
          @endauth
          
        </div>
      </div>
    </li>
    @auth
    <li class="nav-item">
      <a class="nav-link" href="/peminjaman">
        <i class="far fa-clipboard"></i>
        <span>Peminjaman</span>
      </a>
    </li>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
      Logout
    </div>
    <li class="nav-item">
          <a class="nav-link" href="{{ route('logout') }}" 
          onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i>
          <span>
            Logout
          </span>
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
    </li>

  </ul>    
    @endauth
    @guest
    <li class="nav-item">
      <a class="nav-link" href="/login">
      <i class="fas fa-sign-out-alt"></i>
      <span>
        Login
      </span>
      </a>

</li>

</ul> 
    @endguest
    
  <!-- Sidebar -->