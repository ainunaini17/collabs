@extends('layout.master')

@section('judul')
Peminjaman Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Daftar Peminjaman</a></li>
    <li class="breadcrumb-item"><a href="/peminjaman/edit">Edit Peminjaman</a></li>
  </ol>
@endsection

@section('content')

<button type="button" name="save" class="btn btn-primary mb-3" data-toggle="modal" data-target="#modal-item">Tambah Data</button>
<div class="table-responsive p-3">
    <table class="table align-items-center table-flush" id="dataTable">
      <thead class="thead-light">
        <tr>
          <th>#</th>
          <th>Nama Peminjam</th>
          <th>Buku yang Dipinjam</th>
          <th>Tanggal Peminjaman</th>
          <th>Tanggal Pengembalian</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <td>1</td>
            <td>Nama Peminjam</td>
            <td>Buku yang Dipinjam</td>
            <td>Tanggal Peminjaman</td>
            <td>Tanggal Pengembalian</td>
            <td>
                <form action="#" method="POST">
                    <a href="/peminjaman/edit" class="btn btn-warning btn-sm" role="button" >Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
                </td>
        </tr>
      </tbody>
    </table>
    <!-- Modal -->
    <div class="modal fade" id="modal-item">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-item">Tambah Data Peminjaman</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-user"></i>
                                        <label for="user_id">Nama Peminjam</label>
                                        <div class="position-relative">
                                        <select name="user_id" class="form-control form-select" id="">
                                            <option value="">---Pilih User---</option>
                                            <option value="dini">Dini</option>
                                        </select>
                                        </div>
                                        @error('user_id')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-book-open"></i>
                                        <label for="buku_id">Judul Buku</label>
                                        <div class="position-relative">
                                        <select name="buku_id" class="form-control form-select" id="">
                                            <option value="">---Pilih Judul Buku---</option>
                                            <option value="Pulang">Pulang</option>
                                        </select>
                                        </div>
                                        @error('buku_id')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-calendar-alt"></i>
                                        <label for="tgl_pinjam">Tanggal Peminjaman</label>
                                        <div class="position-relative">
                                            <input type="date" name="tgl_pinjam" id="tgl_pinjam" class="form-control" placeholder="Isi tgl pinjam Buku" autofocus>
                                        </div>
                                        @error('tgl_pinjam')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group has-icon-left">
                                        <i class="fas fa-calendar-alt"></i>
                                        <label for="tgl_pengembalian">Tanggal Pengembalian</label>
                                        <div class="position-relative">
                                            <input type="date" name="tgl_pengembalian" id="tgl_pengembalian" class="form-control" placeholder="Isi tgl pengembalian Buku" autofocus>
                                        </div>
                                        @error('tgl_pengembalian')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" name="save" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection