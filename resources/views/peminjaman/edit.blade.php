@extends('layout.master')

@section('judul')
Peminjaman Buku
@endsection

@section('header')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/peminjaman">Daftar Peminjaman</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Peminjaman</li>
  </ol>
@endsection

@section('content')
    <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group has-icon-left">
                        <i class="fas fa-user"></i>
                        <label for="user_id">Nama Peminjam</label>
                        <div class="position-relative">
                        <select name="user_id" class="form-control form-select" id="">
                            <option value="">---Pilih User---</option>
                            <option value="dini">Dini</option>
                        </select>
                        </div>
                        @error('user_id')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group has-icon-left">
                        <i class="fas fa-book-open"></i>
                        <label for="buku_id">Judul Buku</label>
                        <div class="position-relative">
                        <select name="buku_id" class="form-control form-select" id="">
                            <option value="">---Pilih Judul Buku---</option>
                            <option value="Pulang">Pulang</option>
                        </select>
                        </div>
                        @error('buku_id')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group has-icon-left">
                        <i class="fas fa-calendar-alt"></i>
                        <label for="tgl_pinjam">Tanggal Peminjaman</label>
                        <div class="position-relative">
                            <input type="date" name="tgl_pinjam" id="tgl_pinjam" class="form-control" placeholder="Isi tgl pinjam Buku" autofocus>
                        </div>
                        @error('tgl_pinjam')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group has-icon-left">
                        <i class="fas fa-calendar-alt"></i>
                        <label for="tgl_pengembalian">Tanggal Pengembalian</label>
                        <div class="position-relative">
                            <input type="date" name="tgl_pengembalian" id="tgl_pengembalian" class="form-control" placeholder="Isi tgl pengembalian Buku" autofocus>
                        </div>
                        @error('tgl_pengembalian')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button>
            <button type="submit" name="save" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection
