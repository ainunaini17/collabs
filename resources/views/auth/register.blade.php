<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="{{asset('admin/img/logo/logo.png')}}" rel="icon">
  <title>Register</title>
  <link href="{{asset('admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/css/ruang-admin.min.css')}}" rel="stylesheet">

</head>

<body class="bg-gradient-login">
  <!-- Register Content -->
  <div class="container-login">
    <div class="row justify-content-center">
      <div class="col-xl-5 col-lg-12 col-md-9">
        <div class="card shadow-sm my-5">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-lg-12">
                <div class="login-form">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><b>Register</b></h1>
                  </div>

                  <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="form-group">
                      <label>Full Name</label>
                      <input type="text" name="name" class="form-control" id="exampleInputFirstName" placeholder=" Full Name">
                    </div>
                    @error('name')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror                  


                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" name="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp"
                        placeholder="Masukkan Email">
                    </div>
                    @error('email')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    <div class="form-group">
                      <label>Password</label>
                      <input type="password" name="password" class="form-control" id="exampleInputPassword" placeholder="Password">
                    </div>
                    @error('password')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    <div class="form-group">
                      <label>Repeat Password</label>
                      <input type="password" name="password_confirmation" class="form-control" id="exampleInputPasswordRepeat"
                        placeholder="Repeat Password">
                    </div>

                    <div class="form-group">
                      <label >Umur</label>
                      <input type="number" name="umur" class="form-control" placeholder="Masukkan Umur">
                    </div>
                    @error('umur')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    <div class="form-group">
                      <label >Alamat</label>
                      <textarea name="alamat" class="form-control" placeholder="Masukkan Alamat" cols="15" rows="10"></textarea>                   
                    </div>
                    @error('alamat')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    <div class="form-group">
                      <label >Nomor Handphone</label>
                      <input type="number" name="nohp" class="form-control" placeholder="Masukkan Nomor Handphone">
                    </div>
                    @error('nohp')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    <div class="form-group mt-5">
                      <button type="submit" class="btn btn-primary btn-block">Register</button>
                    </div> 
                  </form>       
                  
                  <hr>
                  <div class="text-center">
                    <a class="font-weight-bold small" href="/login">Already have an account?</a>
                  </div>
                 
                  <div class="text-center">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Register Content -->
  <script src="{{asset('admin/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('admin/js/ruang-admin.min.js')}}"></script>
</body>

</html>