<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('guest/index');
// });
// Route::get('/book', function () {
//     return view('guest/book');
// });
// Route::get('/form', function () {
//     return view('guest/form');
// });
// Route::get('/about', function () {
//     return view('guest/about');
// });


// // login & regist
// Route::get('/login', function () {
//     return view('auth/login');
// });
// Route::get('/register', function () {
//     return view('auth/register');
// });

// view
Route::get('/', function () {
    return view('home/dashboard');
});


// peminjaman
Route::get('/peminjaman', function () {
    return view('peminjaman/index');
});
Route::get('/peminjaman/edit', function () {
    return view('peminjaman/edit');
});

// // CRUD Kategori
// Route::get('/kategori/create', 'KategoriController@create');
// Route::post('/kategori', 'KategoriController@store');
// Route::get('/kategori', 'KategoriController@index');
// Route::get('/kategori/{kategori_id}', 'KategoriController@show');
// Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
// Route::put('/kategori/{kategori_id}', 'KategoriController@update');
// Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');


// profile



// Auth
Auth::routes();



// Route::resource('buku', 'BukuController');

Route::get('/buku', 'BukuController@index');
Route::get('/buku/{buku_id}', 'BukuController@show');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('kategori', 'KategoriController');
    Route::resource('profile', 'ProfileController')->only(['index', 'update']);
    Route::get('/buku/{buku_id}/edit', 'BukuController@edit');
    Route::put('/buku/{buku_id}', 'BukuController@update');
    Route::delete('/buku/{buku_id}', 'BukuController@destroy');
    Route::get('/buku/create', 'BukuController@create');
Route::post('/buku', 'BukuController@store');
});
