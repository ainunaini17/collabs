<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Buku;
use File;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();

       return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        return view ('buku.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'penulis' => 'required',
            'sinopsis' => 'required',
            'sampul' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'kategori_id' => 'required',
        ]);


        $SampulName = time().'.'.$request->sampul->extension();

        $request->sampul->move(public_path('gambar'), $SampulName);

        $buku = new Buku;

        $buku  ->judul = $request->judul;
        $buku  ->penulis = $request->penulis;
        $buku  ->sinopsis = $request->sinopsis;
        $buku  ->sampul = $SampulName;
        $buku  ->kategori_id = $request->kategori_id;

        $buku  ->save();

        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::findOrFail($id);

        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->get();
        $buku = Buku::findOrFail($id);

        return view('buku.edit', compact('buku','kategori'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'penulis' => 'required',
            'sinopsis' => 'required',
            'sampul' => 'image|mimes:jpeg,png,jpg|max:2048',
            'kategori_id' => 'required',
        ]);

        $buku = Buku::find($id);

        if($request->has('sampul')){

            $SampulName = time().'.'.$request->sampul->extension();

            $request->sampul->move(public_path('gambar'), $SampulName);
    
            
            $buku  ->judul = $request->judul;
            $buku  ->penulis = $request->penulis;
            $buku  ->sinopsis = $request->sinopsis;
            $buku  ->sampul = $SampulName;
            $buku  ->kategori_id = $request->kategori_id;    

        }else{
            
            $buku  ->judul = $request->judul;
            $buku  ->penulis = $request->penulis;
            $buku  ->sinopsis = $request->sinopsis;
            $buku  ->kategori_id = $request->kategori_id;    

        }

        
        $buku->update();

        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::find($id);

        $path = "gambar/";
        File::delete($path . $buku->sampul);
        $buku->delete();

        return redirect('/buku');
    }
}
