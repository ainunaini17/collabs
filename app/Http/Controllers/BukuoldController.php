<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Kategori;
use Illuminate\Http\Request;
use File;

class BukuController extends Controller
{
    
    public function index(){
        $buku= Buku::all();
        $kategori = Kategori::all();
        return view('buku.index', compact('buku','kategori'));
    }
    public function create(){
        $kategori = Kategori::all();
        return view('buku.create',compact('kategori'));
    }
    public function store(Request $request){
        $request->validate([
            'judul'=>'required|min:5',
            'penulis'=>'required',
            'sinopsis'=>'required',
            'sampul'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'kategori_id'=>'required'
        ]);
        $sampulName = time(). '.' . $request->sampul->extension();
        $request->sampul->move(public_path('gambar'),$sampulName);
        Buku::create([
            'judul' => $request->judul,
            'penulis'=> $request->penulis,
            'sinopsis' => $request->sinopsis,
            'sampul'=> $sampulName,
            'kategori_id'=> $request->kategori_id
        ]);
        return redirect('/buku');
    }
    public function show($id){
        $buku = Buku::FindOrFail($id);
        return view('buku.detail', compact('buku'));
    }
    public function edit($id){
        $buku = Buku::FindOrFail($id);
        $kategori = Kategori::all();
        return view('buku.edit', compact('buku','kategori'));
    }
    public function update($id, Request $request){
        if ($request->has('sampul')){
            $sampulName = time(). '.' . $request->sampul->extension();
            $request->sampul->move(public_path('gambar'),$sampulName);
            $buku = Buku::find($id);
            $buku->judul = $request->judul;
            $buku->penulis = $request->penulis;
            $buku->sinopsis = $request->sinopsis;
            $buku->sampul = $sampulName;
            $buku->kategori_id = $request->kategori_id;
            
        }else{
            $buku = Buku::find($id);
            $buku->judul = $request->judul;
            $buku->penulis = $request->penulis;
            $buku->sinopsis = $request->sinopsis;
            $buku->kategori_id = $request->kategori_id;

        }

        $buku->update();
        return redirect('/buku');
    }
    public function destroy($id){
        $buku = Buku::find($id);
        $path = "gambar/";
        File::delete($path . $buku->sampul);
        $buku->delete();
        return redirect('/buku');
        
    }
}
