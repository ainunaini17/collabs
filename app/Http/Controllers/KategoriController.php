<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use DB;

class KategoriController extends Controller
// {
//     public function index(){
//         $kategori= Kategori::all();
//         return view('kategori.index', compact('kategori'));
//     }
//     public function create(){
//         return view('kategori.create');
//     }
//     public function store(Request $request){
//         $request->validate([
//             'nama'=>'required'
//         ]);
//         Kategori::create([
//             'nama' => $request->nama,
//         ]);
//         return redirect('/kategori');
//     }
//     public function show($id){
//         $kategori = Kategori::FindOrFail($id);
//         return view('kategori.detail', compact('kategori'));
//     }
//     public function edit($id){
//         $kategori = Kategori::FindOrFail($id);
//         return view('kategori.edit', compact('kategori'));
//     }
//     public function update($id, Request $request){
//             $kategori = Kategori::find($id);
//             $kategori->nama = $request->nama;
//             $kategori->update();
//             return redirect('/kategori');
//     }
//     public function destroy($id){
//         $kategori = Kategori::find($id);
//         $kategori->delete();
//         return redirect('/kategori');
        
//     }
// }
{
    public function create(){
        return view('kategori.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
           

        ]);

        DB::table('kategori')->insert([
            'nama' => $request['nama'],
           

        ]);

        return redirect('/kategori');

    }

    public function index(){
        $kategori = Kategori::all();

        return view('kategori.index', compact('kategori'));
    }

    public function show($id){
        $kategori = Kategori::find($id);
        return view ('kategori.show', compact('kategori'));
    }

    public function edit($id){
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view ('kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
           

        ]);

        $query = DB::table('kategori')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                           
                ]);

        return redirect('/kategori');

    }

    public function destroy($id){        
        DB::table('kategori')->where('id', $id)->delete();

        return redirect('/kategori');
    }
}

