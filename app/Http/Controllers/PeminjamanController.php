<?php

namespace App\Http\Controllers;
use App\Peminjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PeminjamanController extends Controller
{
    public function store(Request $request){
        
        $request->validate([
            'tgl_pinjam' => 'required',
            'tgl_pengembalian' => 'required',

        ]);
        $peminjaman = new Peminjaman() ;
        $peminjaman->tgl_pinjam = $request->tgl_pinjam;
        $peminjaman->tgl_pengembalian = $request->tgl_pengembalian;
        $peminjaman->user_id = Auth::id();
        $peminjaman->buku_id = $request->buku_id;
        $peminjaman->save();
        return redirect()->back();
    }
}
