<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "buku";
    protected $fillable = ["judul","penulis","sinopsis","sampul","kategori_id"];

    public function kategori(){
        return $this->belongsTo('App\Kategori');
    }
    public function peminjaman(){
        return $this->hasMany('App\Peminjaman');
    }
}
