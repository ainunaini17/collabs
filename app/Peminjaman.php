<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = "peminjaman";
    protected $fillable = ["user_id","buku_id","tgl_pinjam","tgl_pengembalian"];

    public function buku(){
        return $this->belongsTo('App\Buku');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
