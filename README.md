<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Final Project

## Kelompok 1

## Anggota kelompok

-   Mochamad Jafar Aprilreqsa Sidiq (@Aprilreqsa)
-   Muhammad Sukron (@muhsukron)
-   Ratu Anisah Qurrotu'aini (@ratu_anisah17)

## Tema Project

Website peminjaman buku

## ERD

<img src="public/erd.png" alt="Build Status">

## Link Video

-   **[Link Youtube](https://youtu.be/mro8XfzESmE)**
-   **[Link deploy](app-perpustakaan.herokuapp.com)**
